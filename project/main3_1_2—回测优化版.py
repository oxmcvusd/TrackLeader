# coding=utf-8

# 标题：龙头战法
# author：张连重 
# version 3.0.0
#    date: 2021-02-14  21:47
#    description: 基于情绪周期实现  
# version  3.1.0
#    date: 2021-03-06  14:18
#    description:  取消集合竞价买入
# version  3.2.0
#    date: 2021-03-06  19:00
#    description:  加入钉钉机器人通信功能
# version  3.2.1
#    date: 2021-03-10  18:30
#    description:   1.解决情绪周期初始化问题
#                   2.当天已卖出的股票加入黑名单，不再买入

# version 3.2.2
#    date: 2021年3月22日
#    description： 通过矩阵化运算替代掘金之外的逻辑。
#    date: 2021年4月06日
#    description： 通过历史方式获取tick数据，性能再次翻倍。
'''
性能耗时记录：
# 开始时： 耗时2+3=5秒
收盘后运行：end 16:14:45
开盘前运行：start 16:14:47
开盘时运行：start 16:14:42
收盘后运行：start 16:14:45

# 结束时：耗时 40+50=90秒
开盘时运行：start 16:36:14
集合竞价数据为空
收盘后运行：start 16:36:54
收盘后运行：end 16:36:54
开盘前运行：start 16:37:43

'''

# 导入函数库
from gm.api import *
from datetime import datetime
import pandas as pd
from jqdatasdk import *
import numpy  as np
import time
import random
import math
import re
import os
from decimal import Decimal, Context, ROUND_HALF_UP, ROUND_DOWN
from collections import defaultdict
import pickle


# jqdata login
auth('18126355535', 'XXXX')


def roll_func(ser):
    '''rolling.apply 的func  用来计算涨停强度'''
    _ser = ser[ser == 0]
    index = _ser.index[-1] if len(_ser) > 0 else ser.index[0]
    # print(ser)
    return ser.loc[index:].sum()



def code_apply(df):
    '''计算符合要求的涨停股票'''
    global num
    df['paused1'] = df['paused'].shift(1)
    df['close1'] = df['close'].shift(1)
    df['open1'] = df['open'].shift(1)
    df['low1'] = df['low'].shift(1)
    df['volume1'] = df['volume'].shift(1)
    df['volume2'] = df['volume'].shift(2)
    df['close2'] = df['close'].shift(2)
    df['high_limit1'] = df['high_limit'].shift(1)
    df['high_limit2'] = df['high_limit'].shift(2)
    df['flag'] = 0
    # 标记涨停股票
    df_flag1 = df.query('close1==high_limit1')
    df_flag2 = df_flag1.query('not(close2==high_limit2 or close1/low1>1.03)')  # 不计入涨停高度的票，主要是一字板。
    # df_flag = df.query('close1==high_limit1 and close2==high_limit2 or (close1==high_limit1 and close1/low1>1.03 )')
    df.loc[df_flag1.index, 'flag'] = 1  # 正常的涨停 高度设置为1
    df.loc[df_flag2.index, 'flag'] = 0.5  # 符合条件的 近一字板 涨停高度设置为 0.5
    df.loc[df.query('paused>0').index, 'flag'] = 0  # 停牌 涨停高度为0
    # df['flag_roll'] = df['flag'].rolling(15, min_periods=3).apply(roll_func, raw=False)
    # num = num + 1
    # print(df.code.iloc[0],num)
    return df


def date_apply(df, context):
    '''计算涨停高度'''
    ser = df['flag']
    ser = df.query('flag>0')['flag']
    # ser = df['flag']

    df_flag = context.df_flag
    df_flag.loc[ser.index, 'flag'] = ser
    df_flag.loc[df_flag.query('flag==0').index, 'flag_sum'] = 0
    df_flag['flag_sum'] = df_flag.sum(axis=1)

    df_flag['flag'] = 0  # 初始化flag
    context.df_flag = df_flag

    return df_flag.loc[ser.index, 'flag_sum']


# 初始化函数，设定基准等等
def init(context):
    context.security = []  # 存放聚宽股票代码
    context.security_stock_juejin = []  # 存放掘金股票代码
    context.security_observe = []  # 待观察股票
    context.sell_list = []
    context.small_cyc = 3
    context.small_cyc_befor = 3
    context.big_cyc = 3
    context.big_cyc_befor = 3
    context.auction_volume_dict = {}
    context.black_stock_code_list = ['000000', '000000']
    context.black_stock_code_a = 0  # 交易股票代码黑名单,动态加入后，可支持手工操作
    context.black_stock_code_b = 0  # 交易股票代码黑名单,动态加入后，可支持手工操作
    context.limit_up_summary = pd.DataFrame(columns=["股票代码掘金", "股票代码聚宽", "股票名称", "昨日涨停高度", "昨日收盘价"])  # 涨停数统计
    context.limit_up_summary.set_index(["股票代码掘金"], inplace=True)
    context.current_day_selled_stock_set = set()  # 当日卖出股票列表
    context.emotion_cycle = pd.DataFrame(
        {"big_cycle": [3, 3, 3, 3, 3, 3, 3, 3, 3, 3], "small_cycle": [3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
         "date_limit_info": ['', '', '', '', '', '', '', '', '', '']})  # 情绪周期

    # 获取初始化数据
    # 获取所有股票信息，df_stock存放聚宽-掘金代码映射,df_stock_JueJ为掘金-聚宽映射
    context.df_stock = get_all_securities(types=['stock'], date=context.backtest_end_time)
    context.df_stock['code_JueJ'] = context.df_stock.index.map(
        lambda x: 'SHSE.' + x[:6] if x[0] == '6' else 'SZSE.' + x[:6])
    context.df_stock_JueJ = context.df_stock.reset_index().set_index('code_JueJ').rename(columns={'index': 'code'})

    # 获取历史行情+涨停数据信息，计算有些耗时，建议pickle存储，方便debug。
    # context.df_price = get_df_price(context)
    # context.df_price.to_pickle(r'df_price_3_2_2.pkl')
    context.df_price = pd.read_pickle(r'df_price_3_2_2.pkl')

    # 获取涨停高度数据
    context.df_flag_sum = get_flag_sum(context)
    context.df_price = context.df_price.merge(context.df_flag_sum, how='left', on=['date', 'code'])
    context.df_price = context.df_price.merge(context.df_stock.loc[:, ['display_name', 'code_JueJ']], how='left',
                                              left_on=['code'], right_index=True)

    # 获取集合竞价历史数据
    try:
        context.df_auction_all = pd.read_pickle('df_auction_all.pkl')
    except:
        context.df_auction_all = pd.DataFrame()

    context.date_now = context.now.date()
    # 设置date_last，处理date_now非交易日的情况。
    date_list = get_trade_days(end_date=context.date_now,count=2)
    if context.date_now not in date_list:
        context.date_last = date_list[-1]
    else:
        context.date_last = date_list[0]

    init_emotion_cycle(context)
    add_parameter(key='black_stock_code_a', value=context.black_stock_code_a, min=0, max=999999,
                  name='black_stock_code_a', intro='股票黑名单A', group='stock_code', readonly=False)
    add_parameter(key='black_stock_code_b', value=context.black_stock_code_b, min=0, max=999999,
                  name='black_stock_code_b', intro='股票黑名单B', group='stock_code', readonly=False)
    # add_parameter(key='small_cyc', value=context.small_cyc, min=1, max=20, name='samll cycle', intro='情绪小周期', group='emotioncyle', readonly=False)
    # add_parameter(key='small_cyc_befor', value=context.small_cyc_befor, min=1, max=20, name='small_cyc_befor', intro='情绪小周期-before', group='emotioncyle', readonly=False)
    # add_parameter(key='big_cyc', value=context.big_cyc, min=1, max=20, name='big cycle', intro='情绪大周期', group='emotioncyle', readonly=False)
    # add_parameter(key='big_cyc_befor', value=context.big_cyc_befor, min=1, max=20, name='big_cyc_befor', intro='情绪大周期-before', group='emotioncyle', readonly=False)

    # 开盘前运行
    schedule(schedule_func=before_market_open, date_rule='1d', time_rule='09:01:00')

    # 集合竞价尾期运行
    schedule(schedule_func=analyse_call_auction, date_rule='1d', time_rule='09:27:00')

    # 开盘时运行
    schedule(schedule_func=market_open, date_rule='1d', time_rule='09:30:00')

    # 收盘后运行
    schedule(schedule_func=after_market_close, date_rule='1d', time_rule='15:01:00')


# 获取初始化数据
def get_df_price(context):
    '''涨停数据合成'''
    start_time = context.backtest_start_time
    end_time = context.backtest_end_time
    _start = pd.to_datetime(start_time) - pd.to_timedelta('20 days')  # 行情数据要比回测数据早
    df_price = get_price(context.df_stock.index.to_list(), start_date=_start, end_date=end_time,
                         fields=['open', 'close', 'low', 'high', 'volume', 'high_limit', 'low_limit', 'paused'])
    df_price.rename(columns={"time": "date"}, inplace=True)
    df_price = df_price.groupby(['code']).apply(code_apply)
    return df_price


def get_flag_sum(context):
    '''涨停高度数据合成'''
    df_flag = context.df_price.loc[:, ['code', 'flag']]
    df_flag = df_flag.drop_duplicates(['code'])
    df_flag.set_index('code', inplace=True)
    df_flag['flag'] = 0
    df_flag['flag_sum'] = 0
    context.df_flag = df_flag

    df = context.df_price.sort_values(['date', 'code'])
    # date = df_price['date'].iloc[0]
    # df.query('date == @date')
    df = df.set_index('code')
    df_flag_sum = df.groupby('date').apply(date_apply, context)
    return df_flag_sum.reset_index()


# 响应参数修改
def on_parameter(context, parameter):
    # print(parameter)
    if parameter['key'] == 'black_stock_code_a':
        context.black_stock_code_a = math.trunc(parameter['value'])
        black_stock_code_a = '%06d' % context.black_stock_code_a
        context.black_stock_code_list[0] = fmt_code(black_stock_code_a, 'gm')
        print('黑名单列表已修改为' + str(context.black_stock_code_list))
    if parameter['key'] == 'black_stock_code_b':
        context.black_stock_code_b = math.trunc(parameter['value'])
        black_stock_code_b = '%06d' % context.black_stock_code_b
        context.black_stock_code_list[1] = fmt_code(black_stock_code_b, 'gm')
        print('黑名单列表已修改为' + str(context.black_stock_code_list))
    """    
    if parameter['key'] == 'small_cyc':
        context.small_cyc = math.trunc(parameter['value']) 
        print('{}已修改为{}'.format(parameter['name'],context.small_cyc))
    if parameter['key'] == 'big_cyc':
        context.big_cyc = math.trunc(parameter['value']) 
        print('{}已修改为{}'.format(parameter['name'],context.big_cyc))
    if parameter['key'] == 'small_cyc_befor':
        context.small_cyc_befor = math.trunc(parameter['value']) 
        print('{}已修改为{}'.format(parameter['name'],context.small_cyc_befor))
    if parameter['key'] == 'big_cyc_befor':
        context.big_cyc_befor = math.trunc(parameter['value']) 
        print('{}已修改为{}'.format(parameter['name'],context.big_cyc_befor))  
    """


def init_emotion_cycle(context):
    """
    初始化情绪周期参数   T-3 DAY
    """
    trade_dates = get_trade_days(end_date=context.date_now, count=4)
    history_emotion_cycle(context, trade_dates[0])  # T-3
    history_emotion_cycle(context, trade_dates[1])  # T-2
    emotion_cycle_init = {"big_cycle": context.big_cyc, "small_cycle": context.small_cyc}
    print("已自动完成情绪周期初始化" + str(emotion_cycle_init))


def history_emotion_cycle(context, date):

    # _df_stock = get_all_securities(types=['stock'], date=spec_day)
    df_stock = context.df_stock.copy()

    # 过滤上市不足100天的票
    df_stock['days'] = df_stock['start_date'].map(lambda x: (date - x.date()).days)
    df_stock = df_stock.query('days>100')
    stocks = df_stock.index.to_list()


    df2 = context.df_price.query(
        'date==@date and code in @stocks and paused==0 and close>2.2 and close==high_limit')
    stocks = df2['code'].tolist()
    # stocks = df2.query('paused==0 and close>2.2 and close==high_limit')['code'].tolist()


    stocks = st_check(context, date, stocks)  # 过滤st股

    context.limit_up_summary = get_limit_up_summary(context, date, stocks)
    print(date)
    print(context.limit_up_summary)
    context.small_cyc = context.limit_up_summary['昨日涨停高度'].max()
    # context.big_cyc = max(context.small_cyc, context.small_cyc_befor)
    if context.small_cyc <= context.small_cyc_befor:
        context.big_cyc = context.small_cyc_befor
    elif context.small_cyc > context.big_cyc:
        context.big_cyc = context.small_cyc
    context.small_cyc_befor = context.small_cyc
    context.big_cyc_befor = context.big_cyc


def get_limit_up_summary(context, staticEndDate, stock_list):
    """
    获取连板统计
    """


    date = get_trade_days(start_date=staticEndDate)[1]
    df_code = context.df_price.query('date == @date and flag_sum>0 and code in @stock_list')

    # 过滤掉最近2周有停牌的票
    date_start = get_trade_days(end_date=date, count=10)[0]
    df_paused = context.df_price.query('@date_start<=date<=@date and paused==1')
    code_paused_list = df_paused['code'].unique().tolist()
    df_code = df_code.query('code not in @code_paused_list')


    df_select = df_code.loc[:, ['code', 'code_JueJ', 'display_name', 'flag_sum', 'close1']]
    df_select.rename(columns={'code': '股票代码聚宽', 'code_JueJ': '股票代码掘金', 'display_name': '股票名称', 'flag_sum': '昨日涨停高度',
                              'close1': '昨日收盘价'}, inplace=True)
    return  df_select.set_index('股票代码掘金')



def before_market_open(context):
    """
    开盘前运行函数,获取连板目标股票列表
    """
    context.date_now = context.now.date()
    # 输出运行时间
    log(level='info', msg='********新的一天开始**********before_market_open ' + str(context.now), source='TrackLeader')
    print("******新的一天开始****before_market_open " + str(context.now))
    print('开盘前运行：start', time.strftime('%H:%M:%S'))
    # 获得候选股票
    # print(context.now.date(), context.date_now, context.date_last)
    stock_list = get_stocks(context)


    # 统计昨天涨停股票在过去9天内的连扳情况
    init_before_market(context, stock_list, context.date_last)

    # 先取消订阅所有股票
    # unsubscribe(symbols='*', frequency='tick')
    unsubscribe(symbols='*', frequency='60s')

    ## 开盘确定要订阅的股票池
    subscribe_stocks = list(set(context.security_stock_juejin + get_position_list(context)))
    if len(subscribe_stocks) > 0:
        # subscribe(subscribe_stocks, frequency='tick', count=1, wait_group=False, wait_group_timeout='10s',
        #           unsubscribe_previous=True)
        subscribe(subscribe_stocks, frequency='60s', count=2, wait_group=False, wait_group_timeout='10s',
                  unsubscribe_previous=True)
        context.subscribe_stocks = subscribe_stocks # 订阅数据
        print('订阅股票：\n')
        print(context.df_stock.query('code_JueJ in @subscribe_stocks')['display_name'])
        print('=='*10)
    else:
        print('当前没有股票可以订阅')
        print('=='*10)

    # 初始化交易数据
    context.sell_list = []
    context.limit_stocks = []
    context.buy_list_temp = []
    context.auction_volume_dict = {}
    context.current_day_selled_stock_set.clear()
    context.df_auction = pd.DataFrame() # 集合竞价最后3行tick数据
    print('开盘前运行：end', time.strftime('%H:%M:%S'))




def get_stocks(context):
    """
    获得候选股票池  剔除黑名单股票
    """

    _df_stock = context.df_stock.copy()

    # 过滤上市不足160天的票
    _df_stock['days'] = _df_stock['start_date'].map(lambda x: (context.date_now - x.date()).days)
    _df_stock = _df_stock.query('days>160')
    code_list = _df_stock.index.to_list()

    df = context.df_price.query('code in @code_list and date==@context.date_last')
    # 非st(过滤1) + 昨日未停牌 + 收盘价高于2.2 + 收盘涨停
    df = df.query('high_limit/low_limit>1.15 and paused==0 and close>2.2 and close == high_limit')
    # 注意，这种方式过滤非st有问题，创业板st涨跌幅也是20%，这里不考虑了。所以需要通过如下代码加强过滤：
    stocks = df['code'].tolist()
    stocks = st_check(context, context.date_last, stocks)  # 非st(过滤2)

    return stocks


def init_before_market(context, stock_list, staticEndDate):
    """
    统计连续涨停股票,获取今日股票池
    """
    limit_stocks = []
    context.limit_up_summary = get_limit_up_summary(context, staticEndDate, stock_list)
    context.small_cyc = context.limit_up_summary['昨日涨停高度'].max()
    df_data_max = context.limit_up_summary[context.limit_up_summary['昨日涨停高度'] == context.small_cyc]
    df_data_max_sec = context.limit_up_summary[context.limit_up_summary['昨日涨停高度'] == 3]
    print('------------新高股票---------' + str(staticEndDate) + '----')
    print(df_data_max)

    # context.big_cyc = max(context.small_cyc, context.small_cyc_befor)
    if context.small_cyc <= context.small_cyc_befor:
        context.big_cyc = context.small_cyc_befor
    elif context.small_cyc > context.big_cyc:
        context.big_cyc = context.small_cyc

    if context.big_cyc <= 4 and len(df_data_max) < 4:
        # if len(df_data_max) < 4:
        code_list = df_data_max['股票代码聚宽'].tolist()
        # 选出自然板
        df_price = context.df_price.query('code in @code_list and date==@staticEndDate')
        df1 = df_price.query('high_limit>open')
        if len(df1) > 0:
            limit_stocks.extend(df1['code'].to_list())
        # 选出一字板,前一个不能也是一字板 否则获利盘过多，容易追高接盘。这里open1暂时用low1代替
        df2 = df_price.query('high_limit == open and high_limit1>low1')
        if len(df2) > 0:
            df2 = df2.set_index('code')
            df_5m = get_price(df2.index.to_list(), end_date=staticEndDate, frequency='5m', fields=['low'], count=12)
            df2['m5_min'] = df_5m.groupby('code')['low'].min()
            # 一字板最后一个小时拉涨停的，说明不强
            _df = df2.query(' high_limit1==m5_min')
            if len(_df) > 0:
                limit_stocks.extend(_df.index.to_list())


    # 大周期在6及以上，说明当前情绪不错，可以做weiyi3板的股  3板散户获利了结，留下庄家筹码继续拉高
    elif context.big_cyc >= 6 and len(df_data_max_sec) == 1:
        for stock in df_data_max_sec['股票代码聚宽']:
            df = context.df_price.query('code==@stock and date==@staticEndDate and high_limit>open')
            if len(df) > 0:
                limit_stocks.append(stock)



    context.security = limit_stocks
    context.security_stock_juejin = context.limit_up_summary.query('股票代码聚宽 in @limit_stocks').index.to_list()
    context.small_cyc_befor = context.small_cyc
    context.big_cyc_befor = context.big_cyc



def analyse_call_auction(context):
    """
    分析集合竞价情况
    """

    print('集合竞价分析：start', time.strftime('%H:%M:%S'))

    auction_dict = {}
    for code in context.subscribe_stocks:
        # 适用于df_auction_all未初始化的情况
        if len(context.df_auction_all)>0:
            _df = context.df_auction_all.query('symbol==@code and date==@context.date_now')
        else:
            _df = pd.DataFrame()

        if len(_df)==0:
            print(code,context.date_now)
            _df = history(code, 'tick', start_time=str(context.date_now) + ' 09:15:00', end_time=str(context.date_now)+' 09:25:00', df=True)
            # 集合竞价数据一般为200，<20说明当天停牌
            if len(_df) <20:
                continue
            _df['ask_p_info'] = _df['quotes'].map(lambda x: x[0]['ask_p'])
            _df.sort_values('created_at', inplace=True)
            _df['date'] = _df['created_at'].map(lambda x: x.date())
            context.df_auction_all= pd.concat([context.df_auction_all,_df], axis=0)


        auction_dict[code] = _df.iloc[-3:, :]['ask_p_info'].to_list()
        tick = _df['quotes'].iloc[-1]
        buy_sell_volumn = [tick[0]['bid_v'] + tick[1]['bid_v'] + tick[2]['bid_v'],
                           tick[0]['ask_v'] + tick[1]['ask_v'] + tick[2]['ask_v']]
        context.auction_volume_dict[code] = buy_sell_volumn

    if len(auction_dict) == 0:
        print('没有集合竞价数据')
        return
    context.df_auction = pd.DataFrame(auction_dict, index=['a1_p2', 'a1_p1', 'a1_p0']).T


    code_auction_list = list(auction_dict.keys())

    df_price1 = context.df_price.query(
        'code_JueJ in @context.security_stock_juejin and code_JueJ in @code_auction_list and date==@context.date_last')


    df_price1 = df_price1.merge(context.df_auction, left_on='code_JueJ', right_index=True)
    df_buy = df_price1.query('a1_p0/close2<1.5 and a1_p0/close>0.95 and (volume/volume2<3 or a1_p0/close<1.056)')
    security_observe = df_buy.query(' a1_p0<a1_p1 or a1_p1<a1_p2')['code_JueJ'].to_list()
    buy_list_temp = df_buy.query('code_JueJ not in @security_observe')['code_JueJ'].to_list()


    context.security_stock_juejin = buy_list_temp
    context.security = context.df_stock_JueJ.loc[security_observe, 'code'].to_list()
    context.security_observe = security_observe
    stock_opration = {"今日青睐股列表": context.security_stock_juejin, "今日观察股列表": context.security_observe}
    print('集合竞价分析：end', time.strftime('%H:%M:%S'))




def market_open(context):
    """
    开盘时运行函数
    """
    print('开盘时运行：start', time.strftime('%H:%M:%S'))
    # 查询默认交易账户所有持仓
    hold_stocks = get_all_position(context)
    if len(context.df_auction)>0:
        print("今日集合竞价统计数据:\n",context.df_auction)
        print("今日集合竞价买卖方volumn数据:\n", context.auction_volume_dict)
    else:
        print('集合竞价数据为空')

    if len(hold_stocks) == 0:
        print('开盘时运行：end', time.strftime('%H:%M:%S'))
        return


    df_price1 = context.df_price.query('code_JueJ in @hold_stocks and date==@context.date_last')
    df_price1 = df_price1.merge(context.df_auction, left_on='code_JueJ', right_index=True)

    ##分析是否需要卖出现有持仓
    df_code1 = df_price1.query('close/close1 < 1.098 and (a1_p0<a1_p1 or a1_p1<a1_p2 or a1_p0/close<0.98)')
    for code in df_code1['code_JueJ']:
        result = custom_order_target_volume(context, symbol=code, volume=0, position_side=PositionSide_Long,
                                            order_type=OrderType_Market)  # 开盘市价清仓
        context.sell_list.append(code)
        log(level='info', msg='昨天收盘没涨停，今日集合竞价没直接封住，开盘卖出' + str(code), source='TrackLeader')
        print('昨天收盘没涨停，今日集合竞价没直接封住，开盘卖出' + str(code))
        print(result)
        context.current_day_selled_stock_set.add(code)

    df_code2 = df_price1.query('close/close1 >= 1.098 and close==low and close1==low1')
    for code in df_code2['code_JueJ']:
        result = custom_order_target_volume(context, symbol=code, volume=0, position_side=PositionSide_Long,
                                            order_type=OrderType_Market)  # 开盘市价清仓
        print('连续两天一字板，大部分筹码盈利过高，开盘卖出' + str(code))
        print(result)
    print('开盘时运行：end', time.strftime('%H:%M:%S'))



## 盘中运行函数  
#  1、开盘后没封住，跌幅超过%5，盘中卖出
#  2、快收盘时，没封住涨停，盘中卖出
def on_bar(context, bars):
    tim = context.now.strftime("%H:%M:%S")

    # 开盘买入逻辑
    if '09:30:05' <= tim <= '09:45:00':
        on_open(context, bars)

    # 盘中卖出逻辑
    on_close(context, bars)

    # 尾盘平仓逻辑
    if '14:55:00' <= tim <= '15:00:00':
        on_end(context, bars)



def on_open(context, bars):
    """
    盘中下单
    """

    for bar in bars:
        stock = bar['symbol']
        if stock in get_position_list(context):
            continue
        if stock in context.current_day_selled_stock_set:
            continue
            # 购买青睐股
        if len(context.security_stock_juejin) != 0:
            cash = context.account().cash['available']
            total_amount = context.account().cash['nav']
            if cash / total_amount >= 0.2:
                stock_code_jk = context.df_stock_JueJ.loc[stock, 'code']
                # context.df_stock_
                # current_data = get_price(stock_code_jk, end_date=context.now, count=1, frequency='1d',
                #                          fields=['open'])  # 取当日开盘价
                # 注意，下面这行代码只适用于回测，实盘是获取不到该数据的。。。
                open_today = context.df_price.query('code == @stock_code_jk and date==@context.date_now')['open'].iloc[0]


                df_price1 = context.df_price.query('code_JueJ == @stock and date==@context.date_last')
                # 高于开盘价1个点，较昨日收盘价上涨2个点
                if bar['close'] / open_today >= 1.01 and bar['close'] / df_price1['close'].iloc[
                    -1] >= 1.02:
                    print("市价买入青睐股 %s  时间：%s" % (stock, context.now))
                    buy_cash = cash / len(context.security_stock_juejin)
                    result = custom_order_assign_value(context, symbol=stock, value=buy_cash,
                                                       order_type=OrderType_Market)
                    print(result)

        # 购买待观察股
        if len(context.security_observe) == 0:
            continue
        cash = context.account().cash['available']
        total_amount = context.account().cash['nav']
        if cash / total_amount >= 0.25:
            # 可用资金大于总资金25%,才购买待观察股，最多持仓3只股
            buy_cash = cash / len(context.security_observe)
            if stock in context.security_observe:
                level_data = context.df_auction_all.query('symbol==@stock and date==@context.date_now')['ask_p_info'].to_list()
                a1_p_min = min(level_data[-1 * len(level_data) // 2:])
                stock_code_jk = context.df_stock_JueJ.loc[stock,'code']
                # current_data = get_price(stock_code_jk, end_date=context.now, count=1, frequency='1d',
                #                          fields=['open'])
                # 注意，下面这行代码只适用于回测，实盘是获取不到该数据的。。。
                open_today = context.df_price.query('code == @stock_code_jk and date==@context.date_now')['open'].iloc[0]
                df_price1 = context.df_price.query('code_JueJ == @stock and date==@context.date_last')

                if bar['close'] > a1_p_min and bar['close'] / open_today >= 1.01 and bar[
                    'close'] / df_price1['close'].iloc[-1] >= 1.04:
                    log(level='info', msg="市价买入待观察股 %s 时间：%s" % (stock, context.now), source='TrackLeader')
                    print("市价买入待观察股 %s 时间：%s" % (stock,context.now))
                    result = custom_order_assign_value(context, symbol=stock, value=buy_cash,
                                                       order_type=OrderType_Market)
                    if result != None and result[0]['ord_rej_reason'] == 0:
                        context.security_observe.remove(stock)  # 买入成功后从列表删除
                    print(result)


def on_close(context, bars):
    """
    监听盘中情况,卖出
    """

    for bar in bars:
        stock = bar['symbol']
        if stock in get_all_position(context):
            stop_line = 0.92
            # 获取股票的收盘价
            df_price1 = context.df_price.query('code_JueJ == @stock and date==@context.date_last')

            # 如果已连涨3天
            if len(df_price1.query('close/close1>1.12'))>0:
                stop_line =0.85

            if bar['close'] / df_price1['close'].iloc[0] < stop_line:
                df_position = get_df_position(context)
                active_count = df_position.loc[stock]['可用持仓']
                if active_count > 0:
                    result = custom_order_volume(context, symbol=stock, volume=active_count, side=OrderSide_Sell,
                                                 order_type=OrderType_Market, position_effect=PositionEffect_Close)
                    context.sell_list.append(stock)
                    print('跌幅超过8%，盘中卖出' + str(stock) + ' ' + str(active_count) + '股')
                    print(result)
                    context.current_day_selled_stock_set.add(stock)


def on_end(context, bars):
    """
    监听尾盘情况
    """

    df_position = get_df_position(context)
    for bar in bars:
        stock = bar['symbol']
        if stock in get_all_position(context):
            active_count = df_position.loc[stock]['可用持仓']
            if active_count > 0:

                df_price1 = context.df_price.query('code_JueJ == @stock and date==@context.date_last')
                close_price_yesterday = df_price1['close'].iloc[0]

                current_close_price = bar['close']
                if current_close_price / close_price_yesterday < 1.098:
                    print('尾盘没有涨停，市价清空可用持仓' + str(stock) + ' ' + str(active_count) + '股')
                    log(level='info', msg='尾盘没有涨停，市价清空可用持仓' + str(stock) + ' ' + str(active_count) + '股',
                        source='TrackLeader')
                    result = custom_order_volume(context, symbol=stock, volume=active_count, side=OrderSide_Sell,
                                                 order_type=OrderType_Market, position_effect=PositionEffect_Close)
                    print(result)
                    context.current_day_selled_stock_set.add(stock)
                elif current_close_price / close_price_yesterday > 1.11 and current_close_price / close_price_yesterday < 1.195:
                    log(level='info',
                        msg='尾盘涨停，相比昨日剧烈收盘,波动过大，市价清空可用持仓' + str(stock) + ' ' + str(active_count) + '股',
                        source='TrackLeader')
                    print('尾盘涨停，相比昨日剧烈收盘,波动过大，市价清空可用持仓' + str(stock) + '' + str(active_count) + '股')
                    result = custom_order_volume(context, symbol=stock, volume=active_count, side=OrderSide_Sell,
                                                 order_type=OrderType_Market, position_effect=PositionEffect_Close)
                    print(result)
                    context.current_day_selled_stock_set.add(stock)
                else:
                    log(level='info', msg='---尾盘涨停不卖出--，stock_code = ' + str(stock), source='TrackLeader')
                    print('---尾盘涨停不卖出--，stock_code = ' + str(stock))


def after_market_close(context):
    """
    收盘后运行函数
    """
    print('收盘后运行：start', time.strftime('%H:%M:%S'))
    ## 收盘取消所有订阅的数据
    log(level='info', msg='盘后运行after_market_close:' + str(context.now), source='TrackLeader')
    # 取消订阅
    # unsubscribe(symbols='*', frequency='tick')
    unsubscribe(symbols='*', frequency='60s')
    # 当前持仓统计
    df_position = get_df_position(context)
    if len(df_position) >0:
        print('当前持仓：')
        print(get_df_position(context))
    else:
        print('当前空仓')
    log(level='info', msg='一天结束', source='TrackLeader')
    # send_message('美好的一天~')

    context.date_last = context.now.date()
    print('收盘后运行：end', time.strftime('%H:%M:%S'))
    log(level='info', msg='##############################################################', source='TrackLeader')
    print('##############################################################')



def on_backtest_finished(context, indicator):
    print(indicator)
    context.df_auction_all.to_pickle('df_auction.pkl')


def st_check(context, date, security_list):
    """
    去除ST股票
    """
    # current_data = get_extras('is_st', security_list.tolist(), end_date=date, df=True, count=1)
    current_data = get_extras('is_st', security_list, end_date=date, df=True, count=1)
    security_list = [stock for stock in security_list if not current_data[stock][-1]]
    # 返回结果
    return security_list





def fmt_code(code: str, style: str = None):
    """
    对股票代码格式化处理
    ---
    :param code: 股票代码
    :param style: 代码风格
    """
    pattern = re.compile(r"\d+")
    code = pattern.findall(code)[0]
    if style in ["jq", "joinquant", "聚宽"]:
        return code + ".XSHG" if code[0] == "6" else code + ".XSHE"
    if style in ["wd", "windcode", "万得"]:
        return code + ".SH" if code[0] == "6" else code + ".SZ"
    if style in ["gm", "goldminer", "掘金"]:
        return "SHSE." + code if code[0] == "6" else "SZSE." + code
    if style in ["ss", "skysoft", "天软"]:
        return "SH" + code if code[0] == "6" else "SZ" + code
    if style in ["ts", "tushare", "挖地兔"]:
        return code + ".SH" if code[0] == "6" else code + ".SZ"
    else:
        return code






def get_all_position(context):
    """
    获取当前所有持仓股票
    """
    in_stock = []
    Account_positions = context.account().positions()
    for position in Account_positions:
        in_stock.append(position['symbol'])
    return in_stock




def get_df_position(context):
    """
    获取当前持仓情况 股票代码  股票名称 总持仓 今日持仓 可用持仓 成本价 收益值
    """
    df_position = pd.DataFrame(columns=["股票代码", "股票名称", "总持仓", '今日持仓', '可用持仓', "成本价"])
    stock_name = []
    stock_code = []
    total_count = []
    today_count = []
    active_count = []
    cost = []
    Account_positions = context.account().positions()
    for position in Account_positions:

        stock_code.append(position['symbol'])
        stock_name.append(context.df_stock_JueJ.loc[position['symbol'],'display_name'])
        total_count.append(position['volume'])
        today_count.append(position['volume_today'])
        active_count.append(position['volume'] - position['volume_today'])
        cost.append(position['vwap'])

    df_position['股票代码'] = list(stock_code)
    df_position['股票名称'] = list(stock_name)
    df_position['总持仓'] = list(total_count)
    df_position['今日持仓'] = list(today_count)
    df_position['可用持仓'] = list(active_count)
    df_position['成本价'] = list(cost)
    df_position.set_index(["股票代码"], inplace=True)
    return df_position


def get_position_list(context):
    """"
    获取持仓股票列表
    """
    in_stock = []
    Account_positions = context.account().positions()
    for position in Account_positions:
        in_stock.append(position['symbol'])
    return in_stock




def custom_order_volume(context, symbol, volume, side, order_type, position_effect,
                        price=0, order_duration=OrderDuration_Unknown, order_qualifier=OrderQualifier_Unknown,
                        account=''):
    """
    按定量委托
    """
    if symbol in context.black_stock_code_list:
        return None
    return order_volume(symbol, volume, side, order_type, position_effect, price, order_duration, order_qualifier,
                        account)


def custom_order_percent(context, symbol, percent, side, order_type, position_effect,
                         price=0, order_duration=OrderDuration_Unknown, order_qualifier=OrderQualifier_Unknown,
                         account=''):
    # type: (Text, float, int, int, int, float, int, int, Text)->List[Dict[Text, Any]]
    """
    按指定比例委托
    """
    if symbol in context.black_stock_code_list:
        return None
    return order_percent(symbol, percent, side, order_type, position_effect, price, order_duration, order_qualifier,
                         account)


def custom_order_value(context, symbol, value, side, order_type, position_effect,
                       price=0, order_duration=OrderDuration_Unknown, order_qualifier=OrderQualifier_Unknown,
                       account=''):
    # type:(Text, float, int, int, int, float, int, int, Text) ->List[Dict[Text, Any]]
    """
    按指定价值委托
    """
    if symbol in context.black_stock_code_list:
        return None
    return order_value(symbol, value, side, order_type, position_effect, price, order_duration, order_qualifie, account)


def custom_order_assign_value(context, symbol, value, order_type, price=0):
    # type: (Text, float, int, int, float, int, int, Text) ->List[Dict[Text, Any]]
    """
    指定金额买入,考虑到实盘时 券商以涨停价冻结资金，买入场景下转换为指定股数买入
    """
    if symbol in context.black_stock_code_list:
        return None
    if order_type == OrderType_Market:
        # 从context中提取昨日收盘价，计算可买入股数
        order_limit_price_temp = str(context.limit_up_summary.at[symbol, '昨日收盘价'] * 1.10)
        order_limit_price = float(str(Decimal(order_limit_price_temp).quantize(Decimal('0.00'), ROUND_HALF_UP)))
        target_volumn = int(value / order_limit_price) // 100 * 100
        if target_volumn >= 100:
            return order_volume(symbol, target_volumn, side=PositionSide_Long, order_type=OrderType_Market,
                                position_effect=PositionEffect_Open)
        else:
            return None
    elif order_type == OrderType_Limit:
        target_volumn = int(value / price)
        if target_volumn >= 100:
            return order_volume(symbol, target_volumn, side=PositionSide_Long, order_type=OrderType_Limit,
                                position_effect=PositionEffect_Open, price=price)
        else:
            return None


def custom_order_target_percent(context, symbol, percent, position_side, order_type, price=0,
                                order_duration=OrderDuration_Unknown,
                                order_qualifier=OrderQualifier_Unknown, account=''):
    # type: (Text, float, int, int, float, int, int, Text) ->List[Dict[Text, Any]]
    """
    调仓到目标持仓比例
    """
    if symbol in context.black_stock_code_list:
        return None
    return order_target_percent(symbol, percent, position_side, order_type, price, order_duration, order_qualifier,
                                account)


def custom_order_target_volume(context, symbol, volume, position_side, order_type, price=0,
                               order_duration=OrderDuration_Unknown,
                               order_qualifier=OrderQualifier_Unknown, account=''):
    """
    调仓到目标持仓量
    """
    if symbol in context.black_stock_code_list:
        return None
    return order_target_volume(symbol, volume, position_side, order_type, price, order_duration, order_qualifier,
                               account)


if __name__ == '__main__':
    '''
    strategy_id策略ID,由系统生成
    filename文件名,请与本文件名保持一致
    mode实时模式:MODE_LIVE回测模式:MODE_BACKTEST
    token绑定计算机的ID,可在系统设置-密钥管理中生成
    backtest_start_time回测开始时间
    backtest_end_time回测结束时间
    backtest_adjust股票复权方式不复权:ADJUST_NONE前复权:ADJUST_PREV后复权:ADJUST_POST
    backtest_initial_cash回测初始资金
    backtest_commission_ratio回测佣金比例
    backtest_slippage_ratio回测滑点比例
    '''
    run(strategy_id='899bad8b-817c-11eb-b734-00fff1c18d34',
        filename='main3_2_2.py',
        mode=MODE_BACKTEST,
        token='77dd981f3a615985c750d44742052009e3e8daf6',
        backtest_start_time='2021-01-01 08:00:00',
        # backtest_start_time='2021-02-25 08:00:00',
        backtest_end_time='2021-03-09 09:00:00',
        backtest_adjust=ADJUST_PREV,
        backtest_initial_cash=50000,
        backtest_commission_ratio=0.00025,
        backtest_slippage_ratio=0.0001)
